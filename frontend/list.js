
var recipes = [
    {"Vegetarian Tacos":{"page_link":"detail.html", "image_link":"https://mks.io/public-tacos", "rating":"4.3/5 - ", "ingredients": ["1 Cup - Green Beans", "3 Ears - Fresh Corn", "2 Cups - Tomatillo salsa", "8 - Corn Tortillas", "1/2 Cup - Cotija Cheese"]}},

    {"Loaded Potato Soup":{"page_link":"detail.html", "image_link":"https://www.budgetbytes.com/wp-content/uploads/2022/02/Loaded-Potato-Soup-V2-768x1024.jpg", "rating":"4.3/5 - ",
     "ingredients": ["10 - Idaho Potatoes ", "96 oz - Chicken Stock", "2 Cups - Baking Flower", "2 Cups - Butter", "Salt", "Pepper", "Cheddar Cheese", "Sour Cream", "Green Onion", "Bacon"]}}
]

let recipe_div = document.querySelector('#all_recipes');

for (let i=0; i<recipes.length; i++) {
    let current_recipe = recipes[i]
    let name = Object.keys(current_recipe)[0]
    let data = current_recipe[name]

    let template = document.querySelector('#recipe_object');

    let clone = template.content.cloneNode(true);
    let div_name = clone.querySelector(".recipe_card_inner")
    div_name.setAttribute("name", name)
    let link_tag = clone.querySelector("a");
    link_tag.textContent = name;
    link_tag.setAttribute("href", data.page_link);

    let image = clone.querySelector("img");
    image.setAttribute("src", data.image_link);

    let rating = clone.querySelector(".rating_val");
    rating.textContent = data.rating;

    let back_side_name = clone.querySelector(".rcb_name");
    back_side_name.textContent = name;
    let ingredient_list = clone.querySelector(".rcb_ingredients");

    let ingredients = data.ingredients

    for (let i=0; i<ingredients.length; i++) {
        let list_item = document.createElement("li");
        list_item.textContent = ingredients[i]
        ingredient_list.append(list_item)
    }

    recipe_div.append(clone);
};
console.log(recipe_div)